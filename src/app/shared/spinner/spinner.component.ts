import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mor-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css'],
  inputs: ['visible']
})
export class SpinnerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}