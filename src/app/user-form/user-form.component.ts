import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {User} from '../user/user';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'mor-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  @Output() userAddEvent = new EventEmitter<User>();

user:User= {name:'', email:''};

 // onSubmit(){
   // console.log('it worked');
  //}
onSubmit(form:NgForm){
    console.log(form);
    this.userAddEvent.emit(this.user);
    this.user = {
       name: '',
       email: ''
    }
  }
  constructor() { }

  ngOnInit() {
  }

}