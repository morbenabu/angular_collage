import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts/posts.service';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { PostFormComponent } from './post-form/post-form.component';
import {AngularFireModule} from 'angularfire2';
import { ProductsComponent } from './products/products.component';
import { ProductsService } from './products/products.service';
import { ProductComponent } from './product/product.component';
import { ProductsJoinComponent } from './products-join/products-join.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { InvoicesService } from './invoices/invoices.service';
import { CustomersService } from './customers/customers.service';
import { CustomersComponent } from './customers/customers.component';
import { CustomerComponent } from './customer/customer.component';

export const firebaseConfig = {
  apiKey: "AIzaSyBN7lwOMMzFYAe0TuUjgn7EnVpvc6nikac",
    authDomain: "angular2-c3ae4.firebaseapp.com",
    databaseURL: "https://angular2-c3ae4.firebaseio.com",
    storageBucket: "angular2-c3ae4.appspot.com",
    messagingSenderId: "47373991599"
}

const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'posts', component: PostsComponent },
   { path: 'products', component:  ProductsComponent },
    { path: 'Invoice-Form', component: InvoiceFormComponent},
     { path: 'Invoices', component:   InvoicesComponent },
      { path: 'Customers', component: CustomersComponent },
  { path: '', component: CustomersComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    UserFormComponent,
    PostFormComponent,
    ProductsComponent,
    ProductComponent,
    ProductsJoinComponent,
    InvoiceFormComponent,
    InvoicesComponent,
    InvoiceComponent,
    CustomersComponent,
    CustomerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService,PostsService,ProductsService,InvoicesService, CustomersService],
  bootstrap: [AppComponent]
})
export class AppModule { }