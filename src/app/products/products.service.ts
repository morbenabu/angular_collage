import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';

@Injectable()
export class ProductsService {
  productsObservable;

 // getProducts(){
   //  this.productsObservable = this.af.database.list('/product');
//     return this.productsObservable;
 // }

  getProducts(){
    this.productsObservable = this.af.database.list('/product').map(
      products =>{
      {products.map(
         product => {
            product.categoryName = [];
            //for(var p in product.category){
                product.categoryName.push(
                this.af.database.object('/category/' + product.categoryId));//p)
            //  )
            //}
          });
        return products;
      }}
    );
   return this.productsObservable; 
   }


  deleteProduct(product){
     let productKey = product.$key;
    this.af.database.object('/product/' + productKey).remove();
  }

  constructor(private af:AngularFire) { }

}
