import { Component, OnInit } from '@angular/core';
import {InvoicesService} from './invoices.service';
//import {InvoiceFormComponent} from '../invoice-form/invoice-form.component';
import {Invoice} from '../invoice/invoice';


@Component({
  selector: 'mor-invoices',
  templateUrl: './invoices.component.html',
  styles: [`
    .invoices td { cursor: default; }
    .invoices td:hover { background: #ecf0f1};
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }      
  `]
})
export class InvoicesComponent implements OnInit {

  currentInvoice;
  isLoading: Boolean = true;

  invoices;
  select(invoice){
		this.currentInvoice = invoice; 
    console.log(this.currentInvoice);
 }

  addInvoice(invoice){
    this._invoicesService.addInvoice(invoice);
  }

  

  constructor(private _invoicesService: InvoicesService) { 
     //this.invoices= this._InvoicesService.getInvoices();
  }

  ngOnInit() {
    this._invoicesService.getInvoices()
    .subscribe(invoicesData => 
    {this.invoices = invoicesData;
       this.isLoading = false});
      //console.log(this.invoices)});
  }

}
