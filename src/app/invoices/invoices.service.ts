import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';

@Injectable()
export class InvoicesService {
  invoicesObservable;

  getInvoices(){
     this.invoicesObservable = this.af.database.list('/invoices').delay(2000);
     return this.invoicesObservable;
  }

  addInvoice(invoice){
    this.invoicesObservable.push(invoice);
   // console.log('its work');
  }

  constructor(private af:AngularFire) { }

}
