import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2';

@Injectable()
export class PostsService {
 // posts = [
   // {'author':'almog','content':'aaa','title':'a'},
    //{'author':'mockey mouse','content':'bbb','title':'b'},
    //{'author':'hallo kitty','content':'ccc','title':'c'},
    //{'author':'spiderman','content':'ddd','title':'d'}
  //]

 // private _url= "http://jsonplaceholder.typicode.com/posts";
 postObservable;
  getPosts(){
    //return this.posts;
    //return this._http.get(this._url).map(res => res.json()).delay(2000);
      this.postObservable = this.af.database.list('/posts').map(
      posts =>{
        posts.map(
          post => {
            post.userNames = [];
            for(var p in post.users){
                post.userNames.push(
                this.af.database.object('/users/' + p)
              )
            }
          }
        );
        return posts;
      }
    )
   return this.postObservable; 
  }


  addPost(post){
    this.postObservable.push(post);
  }

  updatePost(post){
    let postKey = post.$key;
    let postData = {title: post.title, body: post.body};
    this.af.database.object('/posts/' + postKey).update(postData);
    
  }

  deletePost(post){
     let postKey = post.$key;
    this.af.database.object('/posts/' + postKey).remove();
  }

 // constructor(private _http: Http) { }
 constructor(private af:AngularFire) { }

}