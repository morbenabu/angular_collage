import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';

@Component({
  selector: 'mor-posts',
  templateUrl: './posts.component.html',
 // styleUrls: ['./posts.component.css']
  styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1};
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }      
  `]
})
export class PostsComponent implements OnInit {
  posts;
  currentPost;
  isLoading: Boolean= true;

  deletePost(post){
    //this.posts.splice(this.posts.indexOf, 1);
    this._postsServise.deletePost(post);
  }

  saveChange(post){
    this.posts.splice(this.posts,0);
  }


  select(post){
    this.currentPost= post;
    console.log(this.currentPost);
  }

  addPost(post){
    //this.posts.push(post);
    this._postsServise.addPost(post);
  }

updatePost(post){
  this._postsServise.updatePost(post);
}

  constructor(private _postsServise: PostsService) { 
    //this.posts= this._postsServise.getPosts();
  }

  ngOnInit() {
    this._postsServise.getPosts()
    .subscribe(posts => 
    {this.posts = posts;
      this.isLoading=false});
  }

}