import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Post} from './post';

@Component({
  selector: 'mor-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {
   @Output() deleteEvent = new EventEmitter<Post>();
  @Output() cancleEvent = new EventEmitter<Post>();
  @Output() editEvent = new EventEmitter<Post>();
  post:Post;
  isEdit:Boolean= false;
  editButtonText= "Edit";
  saveTitle;
  saveBody;

sendDelete(){
  this.deleteEvent.emit(this.post);
}

toggleEdit(){
  this.isEdit= !this.isEdit;
  this.isEdit ? this.editButtonText="Save" : this.editButtonText="Edit";
  //אם זה במצב עריכה תשמור את הנתונים של פוסט, אחרת, תביא לי את פוסט עצמו.
  this.isEdit ? this.beforSave() : this.afterSave();
  if(!this.isEdit){
    this.editEvent.emit(this.post);
  }
}

beforSave(){
this.saveTitle= this.post.title;
this.saveBody= this.post.body;
}

afterSave(){
  this.cancleEvent.emit(this.post);
}

cancelEdit(){
  this.post.title=this.saveTitle;
  this.post.body=this.saveBody;
  this.isEdit= !this.isEdit;
  this.isEdit ? this.editButtonText="Save" : this.editButtonText="Edit";
}


  constructor() { }

  ngOnInit() {
  }

}