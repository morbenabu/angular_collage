import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class CustomersService {
  private _url = "http://morbe.myweb.jce.ac.il/api/customers/";


  constructor(private _http: Http) { }

   getCustomerFromRestApi(){
   return this._http.get(this._url).map(res=>res.json()).delay(2000);
  }

}
