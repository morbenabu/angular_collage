import { Component, OnInit} from '@angular/core';
import {CustomersService} from './customers.service';

@Component({
  selector: 'mor-customers',
  templateUrl: './customers.component.html',
  styles: [`
    .customers li { cursor: default; }
    .customers li:hover { background: #ecf0f1};
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }      
  `]
})
export class CustomersComponent implements OnInit {
  customers;
  currentCustomer;
  isLoading: Boolean = true;

  deleteCustomer(customer){
    this.customers.splice(
      this.customers.indexOf(customer),1
    )
  }
   select(customer){
		this.currentCustomer = this.customers[3]; 
    console.log(this.currentCustomer);
 }


  constructor(private _customersService: CustomersService) { }

  ngOnInit() {
    this._customersService.getCustomerFromRestApi()
        .subscribe(customersData => {this.customers = customersData.result;
          this.isLoading = false;
          console.log(this.customers)});
  }


}
