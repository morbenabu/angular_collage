import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2';

@Injectable()
export class UsersService {
 // users = [
   // {'name':'mor','email':'mor@gmail.com'},
    //{'name':'tal','email':'tal@gmail.com'},
    // {'name':'daniel','email':'daniel@gmail.com'}
  //]

 // private _url= "http://jsonplaceholder.typicode.com/users";
 private _url= "http://morbe.myweb.jce.ac.il/api/users";
 userObservable;


getUsers() {
 // return this.users;
 //return this._http.get(this._url).map(res => res.json()).delay(2000);
 this.userObservable= this.af.database.list('/users').map(
   users =>{
     users.map(
       user =>{
         user.postTitles = [];
         for(var m in user.posts){
           user.postTitles.push(
             this.af.database.object('/posts/' + m)
           )
         }
       }
     );
     return users;
   }
 )
 return this.userObservable;
}

addUser(user){
  this.userObservable.push(user);
}

updateUser(user){
  let userKey = user.$key;
  let userData = {name: user.name, email: user.email};
  this.af.database.object('/users/' + userKey).update(userData);
}

deleteUser(user){
  let userKey = user.$key;  
  this.af.database.object('/users/' + userKey).remove();
}

getUsersFromApi(){
  return this._http.get(this._url).map(res=>res.json());
}
  //constructor(private _http: Http) { }
  constructor(private af:AngularFire, private _http: Http) { }

}