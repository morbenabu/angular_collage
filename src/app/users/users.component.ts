import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.service';

@Component({
  selector: 'mor-users',
  templateUrl: './users.component.html',
  //styleUrls: ['./users.component.css']
 styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1};
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }      
  `]
})
export class UsersComponent implements OnInit {
  users;
  //users = [
    //{'name':'mor','email':'mor@gmail.com'},
    //{'name':'tal','email':'tal@gmail.com'},
    //{'name':'daniel','email':'daniel@gmail.com'}
  //]

//מאתחל את קארנט יוזר שיפעל על היוזר השני
 // currentUser = this.users[1];
 currentUser;
 isLoading: Boolean = true;

 deleteUser(user){
  // this.users.splice(this.users.indexOf, 1);
  this._usersService.deleteUser(user);
 }

  // פונקציה זו מקבלת יוזר ומחליפה את היוזר של הקארנט יוזר ביוזר שקיבלה בכל פעם שמקליקים על יוזר (נשלח ע"י דף הHTML)")
  select(user){
		this.currentUser = user; 
    console.log(this.currentUser);
 }

 addUser(user){
   // this.users.push(user);
   this._usersService.addUser(user);
  }
  updateUser(user){
    this._usersService.updateUser(user);
  }

  constructor(private _usersService: UsersService) {
    //this.users= this._usersService.getUsers();
  }

  //ngOnInit() {
   // this._usersService.getUsers()
		//	    .subscribe(users =>
      //     {this.users = users;
        //     this.isLoading = false});
  //}
   ngOnInit() {
        this._usersService.getUsersFromApi()
        .subscribe(usersData => {this.users = usersData.result;
          this.isLoading = false;
          console.log(this.users)});
   }

}