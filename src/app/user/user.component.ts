import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {User} from './user';

@Component({
  selector: 'mor-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']
})
export class UserComponent implements OnInit {
  @Output() deleteEvent = new EventEmitter<User>();
  @Output() editEvent = new EventEmitter<User>();
  user:User;
  isEdit:Boolean= false;
  editButtonText="edit";

  sendDelete(){
    this.deleteEvent.emit(this.user);
  }

toggleEdit(){
  this.isEdit = !this.isEdit;
  this.isEdit ? this.editButtonText = "save" : this.editButtonText= "edit";
  if(!this.isEdit){
    this.editEvent.emit(this.user);
  }
}
  constructor() { }

  ngOnInit() {
  }

}