export class Product {
    pid: string;
    pname: string;
    cost: string;
    categoryId: string;
}
