import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Post} from '../post/post';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'mor-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {

  @Output() postAddEvent = new EventEmitter<Post>();

  post:Post = {title:'', body:''};

  //onSubmit(){
    //console.log('it work')
  //}
onSubmit(form:NgForm){
  console.log(form);
  this.postAddEvent.emit(this.post);
  this.post = {
    title:'',
    body:''
  }

}
  constructor() { }

  ngOnInit() {
  }

}