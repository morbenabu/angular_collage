import { Component, OnInit, Output, EventEmitter  } from '@angular/core';
import {Invoice} from '../invoice/invoice';
import {NgForm} from '@angular/forms';
import {InvoicesService} from '../invoices/invoices.service';

@Component({
  selector: 'mor-invoice-form',
  templateUrl: './invoice-form.component.html',
  styleUrls: ['./invoice-form.component.css']
})
export class InvoiceFormComponent implements OnInit {

 // @Output() invoiceAddEvent = new EventEmitter<Invoice>();


  invoices;
   invoice:Invoice = {name:'', amount:'1000'};
   invoicesObservable;

   onSubmit(form:NgForm){   
  console.log(this.invoice);
    // this.invoiceAddEvent.emit(this.invoice);
    this._invoicesService.addInvoice(this.invoice);
  this.invoice = {
    name: '',
    amount:'1000'
 }

}

 

  constructor(private _invoicesService: InvoicesService) { }

  ngOnInit() {
     this._invoicesService.getInvoices()
    .subscribe(invoicesData => {this.invoices = invoicesData});

     
  }

}
