import { Component, OnInit } from '@angular/core';
import {Invoice} from './invoice';

@Component({
  selector: 'mor-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css'],
  inputs:['invoice']
})
export class InvoiceComponent implements OnInit {
  invoice:Invoice;

  constructor() { }

  ngOnInit() {
  }

}
