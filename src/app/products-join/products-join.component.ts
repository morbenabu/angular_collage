import { Component, OnInit } from '@angular/core';
import {ProductsService} from '../products/products.service';

@Component({
  selector: 'mor-products-join',
  templateUrl: './products-join.component.html',
  styleUrls: ['./products-join.component.css']
})
export class ProductsJoinComponent implements OnInit {
  products;

   deleteProduct(product){
    this._productsServise.deleteProduct(product);
  }

  constructor(private _productsServise: ProductsService) { }

  ngOnInit() {
    this._productsServise.getProducts()
    .subscribe(products => 
    {this.products = products});
  }

}
